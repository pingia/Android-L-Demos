# Android L Demos #

Some demos in android 5.0 with Material Design by Support library.

**Note that this is just a personal test project, not an official project.**

## 注意 ##

**不知道什么原因，这个项目突然就被Git@OSC推荐了，= =！，请你务必注意，这只是一些个人用于实践Android 5.0 Material Design效果的测试用项目，并非是严谨的示例程序，代码中可能包含误导性甚至是错误的部分。对于Android 5.0的新特征，你应该参考Google官方提供的开发者示例项目**

**以下是一些有用的链接：（可能需要翻墙）**

- [官方例程目录](http://developer.android.com/samples)

- [RecyclerView](http://developer.android.com/samples/RecyclerView/index.html)

- [CardView](http://developer.android.com/samples/CardView/index.html)

- [Interpolator](http://developer.android.com/samples/Interpolator/index.html)

- [LNotifications](http://developer.android.com/samples/LNotifications/index.html)

- [RevealEffect](http://developer.android.com/samples/RevealEffectBasic/index.html)

- [FloatingActionButton](http://developer.android.com/samples/FloatingActionButtonBasic/index.html)

- [Clipping](http://developer.android.com/samples/ClippingBasic/index.html)

- [ElevationDrag](http://developer.android.com/samples/ElevationDrag/index.html)

- [ElevationBasic](http://developer.android.com/samples/ElevationBasic/index.html)

- [ActivitySceneTransition](http://developer.android.com/samples/ActivitySceneTransitionBasic/index.html)

## Author ##

TakWolf

[takwolf@foxmail.com](mailto:takwolf@foxmail.com)

[http://takwolf.com](http://takwolf.com)
